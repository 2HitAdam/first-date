*Made with Unity v2019.1.0b10*

## Aims

This project is a learning exercise using Unity’s new ECS, jobbing system, and network layer together. I chose these two experimental APIs because I knew it would be an incredibly steep learning curve, and I wanted a project that showed to potential employers that I could pull something working together despite the tricky conditions. I was also just very curious about the tech!
It's a bit of an odd take, and I realise that. My way of thinking about it went like this: I might, on some level, be a better programmer / problem solver than a lot of other canditates out there. But what I'm likely beat on is just not having worked with every part of the Unity engine, or for a very long time. That's not something I figured I could fix in 5 days, which is originally how long I gave myself to work on this. 

After interviewing with a few employers I found that they were doubting my abilities with C# generally, due to a relatively short backlog of public projects built with it. So I decided to be proactive and address that by making a public code repo illustrating some more advanced concepts.

## Concept

I wanted to make a simple networked first date simulator. The idea came from the first date scene in Mountains' Florence, and asking "what could they be saying?" although of course I wanted to put my own weird spin on the writing. One player would choose a dialogue option, and the next player would choose a response, and back to the first, and so on. I wanted to deliver a playful, surprising interactive conversation bounded by preset options.

## Challenges

As stated above, I took this project on _because_ of the challenges it presented. I would just like to state I would absolutely never do something this risky in a production setting.
- The ECS and networking transport layer are both experimental packages. Meaning some bugs are actually Unity engine / editor bugs to be worked around
- Documentation is either sparse or, in the case of ECS, falling behind the actual latest version of the released API.
- Very few people have been working with them, so solving a problem is not quite as simple as just jumping on stackoverflow
- There was an example of using the networking package with ECS, but it relied on parts of the ECS package that are now deprecated, and working out how to update it involved trawling changelogs and one deprecation FAQ forum post
- Even without all this, implementing the concepts of ECS is a steep learning curve. It requires knowing:
    - the full update cycle and how to order systems within it
    - command buffers and when they execute
    - how to design around components structually, as well as how to turn them into entity archetypes and query them
    - how they are chunked and iterated over (and how not to invalidate chunked arrays while processing them)
    - how to handle parallel processing with the jobbing system
    - and more!
- I also wanted to have the data for dialogue loading and saving to JSON, which I hadn't done in Unity/C# before

## Result

In the week I gave myself to work on this, I got the basic logic working! It's not much of a game yet, really just a bunch of debug text and some buttons, but it lets two players connect to a local server and alternate selecting dialogue options. It shows you what the other player selected when they make their choice. The jobbing code in particular is still clumsy and there's a couple of known bugs, but I'm in a good place to finish the project!

I uploaded the builds folder. You can run two instances side by side, start a server on one of them, then connect the client on both. Then you should be able to select dialogue options in alternating windows.

I am going to finish this project, with a nicer GUI and some code clean-up / bug-squashing.
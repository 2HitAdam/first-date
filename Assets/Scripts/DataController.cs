﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataController : MonoBehaviour
{
    public DialogueData[] dialogueData;
    public static Dictionary<int, DialogueProcessedData> dialogueDataDictionary = new Dictionary<int, DialogueProcessedData>();

    void Start()
    {
        CreateDialogueDataDictionary();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateDialogueDataDictionary()
    {
        int uid = 0;

        RecursiveProcessDialogueDataChildren(dialogueData, -1, ref uid);
    }

    private void RecursiveProcessDialogueDataChildren(DialogueData[] dialogueData, int parentId, ref int uid)
    {

        for (int i = 0; i < dialogueData.Length; ++i)
        {
            uid++; // pre-increment means we start numbering at 1
            // this is for use with our dumb server, which uses 0s as 
            // flags for invalid state

            if (parentId >= 0)
            {
                // update the list of the parent to include this id
                // skip the top level by passing invalid -1
                dialogueDataDictionary[parentId].childNodeIds.Add(uid);
            }

            DialogueData data = dialogueData[i];

            dialogueDataDictionary.Add(uid, new DialogueProcessedData
            {
                id = uid,
                text = data.text
            });

            RecursiveProcessDialogueDataChildren(data.childNodes, uid, ref uid);
        }


    }
}

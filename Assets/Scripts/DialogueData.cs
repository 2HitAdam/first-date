﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueData
{
    public string text;
    public DialogueData[] childNodes;
}

public class DialogueProcessedData
{
    public int id;
    public List<int> childNodeIds = new List<int>();
    public string text;
}
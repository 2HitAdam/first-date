﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
public struct GameplayClientStateComponentData : IComponentData
{
    public int assignedId;
    public bool isActive;
    public int activeNodeId;
}

[Serializable]
public struct GameplayServerStateComponentData : IComponentData
{
    public int activeNodeId;
    public int activeClientId;
    public int playerCount;
}

public struct GameplayServerStateUpdateComponentData : IComponentData
{
    public int assignedId;
}

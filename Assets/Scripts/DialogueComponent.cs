﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[Serializable]
public struct DialogueComponentData : IComponentData
{
    public int id;
}

[Serializable]
public struct DialogueActiveOptionComponentData : IComponentData
{
    public bool isActiveOption;
}

[Serializable]
public struct DialogueRenderComponentData : IComponentData
{
    // might put something here like world/screen coordinates
    // later when we're actually drawing these things rather than just displaying them
    public int speaker;
    public int order;
}

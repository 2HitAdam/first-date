﻿using System;
using System.Net;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Networking.Transport;
using UnityEngine;
using Unity.Burst;
using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

// we don't use barriers anymore, they were renamed buffers
// the buffer is used to execute the creation and destruction of connection entities
// this runs at the end of the fixed update but before the MessageServerSystem
public class MessageEntityCommandBufferSystem : EntityCommandBufferSystem
{
}

// SystemStateComponent to track which drivers have been created and destroyed
public struct MessageDriverStateComponent : ISystemStateComponentData
{
    public bool isServer;
}

// The MessageDriverSystem runs at the beginning of FixedUpdate. It updates the NetworkDriver(s) and accepts new connectins
// creating entities to track the connections
[UpdateBefore(typeof(MessageEntityCommandBufferSystem))]
public class MessageDriverSystem : JobComponentSystem
{
    public UdpCNetworkDriver ServerDriver { get; private set; }
    public UdpCNetworkDriver ClientDriver { get; private set; }

    private MessageEntityCommandBufferSystem m_bufferSystem;

    // this is a way of exposing the command buffer to monobehaviours so I can do things like start servers...
    public static MessageEntityCommandBufferSystem HackyBufferSystemTest { get; private set; }

    private EntityQuery m_serverConnectionQuery;
    private EntityQuery m_driverQuery;
    private EntityQuery m_newDriverQuery;
    private EntityQuery m_destroyedDriverQuery;
    private bool m_serverDriverIsActive;

    protected override void OnCreate()
    {
        m_bufferSystem = World.GetOrCreateSystem<MessageEntityCommandBufferSystem>();
        HackyBufferSystemTest = m_bufferSystem;

        m_serverConnectionQuery = GetEntityQuery(new ComponentType[] { typeof(MessageServerConnectionComponentData) });

        // TODO: I could replace this boilerplate with the Fluent EntityQueryBuilder.With<> and use ForEach in OnUpdate
        m_destroyedDriverQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(MessageDriverStateComponent) },
            Any = new ComponentType[0],
            None = new ComponentType[] { typeof(MessageDriverComponentData) } 
        });

        m_newDriverQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(MessageDriverComponentData) },
            Any = new ComponentType[0],
            None = new ComponentType[] { typeof(MessageDriverStateComponent) }
        });

        m_driverQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(MessageDriverComponentData), typeof(MessageDriverStateComponent) },
            Any = new ComponentType[0],
            None = new ComponentType[0]
        });
    }

    protected override void OnDestroy()
    {
        if (m_serverDriverIsActive)
            ServerDriver.Dispose();
        if (ClientDriver.IsCreated)
            ClientDriver.Dispose();
    }

    // can't use the burst compiler here because commandBuffer is a class type
    struct DriverAcceptJob : IJob
    {
        public UdpCNetworkDriver driver;
        public EntityCommandBuffer commandBuffer;

        public void Execute()
        {
            // Accept all connections and create entities for the new connections using an entity command buffer
            while (true)
            {
                var con = driver.Accept();
                if (!con.IsCreated)
                    break;
                Debug.Log("commandBuffer.AddComponent(serverConnectionEntity, new MessageServerConnectionComponentData { connection = con });");
                Entity serverConnectionEntity = commandBuffer.CreateEntity();
                commandBuffer.AddComponent(serverConnectionEntity, new MessageServerConnectionComponentData { connection = con });
            }
        }
    }

    [BurstCompile]
    struct DriverCleanupJob : IJobForEachWithEntity<MessageServerConnectionComponentData>
    {
        public EntityCommandBuffer.Concurrent commandBuffer;

        public void Execute(Entity connectionEntity, int index, ref MessageServerConnectionComponentData connectionData)
        {
            if(!connectionData.connection.IsCreated)
            {
                // Note: we can't use the debugger, or strings for that matter, while burst compiling
                // Debug.Log("commandBuffer.DestroyEntity(index, connectionEntity);");
                commandBuffer.DestroyEntity(index, connectionEntity);
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        inputDeps.Complete();

        var commandBuffer = m_bufferSystem.CreateCommandBuffer();

        NativeArray<MessageDriverStateComponent> destroyedDriverDataArray = m_destroyedDriverQuery.ToComponentDataArray<MessageDriverStateComponent>(Allocator.TempJob);
        NativeArray<Entity> destroyedDriverEntities = m_destroyedDriverQuery.ToEntityArray(Allocator.TempJob);
        NativeArray<MessageServerConnectionComponentData> serverConnectionDataArray = m_serverConnectionQuery.ToComponentDataArray<MessageServerConnectionComponentData>(Allocator.TempJob);
        NativeArray<Entity> serverConnectionEntities = m_serverConnectionQuery.ToEntityArray(Allocator.TempJob);
        
        // Destroy drivers if the PingDriverComponents were removed
        for (int i = 0; i < destroyedDriverDataArray.Length; ++i)
        {
            if (destroyedDriverDataArray[i].isServer)
            {
                // Also destroy all active connections when the driver dies
                for (int con = 0; con < serverConnectionDataArray.Length; ++con)
                {
                    Debug.Log("commandBuffer.DestroyEntity(serverConnectionEntities[con]);");
                    commandBuffer.DestroyEntity(serverConnectionEntities[con]);
                }
                    
                Debug.Log("Disposing of server");
                ServerDriver.Dispose();
                m_serverDriverIsActive = false;
            }
            else
                ClientDriver.Dispose();
            Debug.Log("commandBuffer.RemoveComponent<MessageDriverStateComponent>(destroyedDriverEntities[i]);");
            commandBuffer.RemoveComponent<MessageDriverStateComponent>(destroyedDriverEntities[i]);
        }
        destroyedDriverDataArray.Dispose();
        serverConnectionDataArray.Dispose();
        serverConnectionEntities.Dispose();
        destroyedDriverEntities.Dispose();

        NativeArray<MessageDriverComponentData> newDriverDataArray = m_newDriverQuery.ToComponentDataArray<MessageDriverComponentData>(Allocator.TempJob);
        NativeArray<Entity> newDriverEntities = m_newDriverQuery.ToEntityArray(Allocator.TempJob);

        // Create drivers if new PingDriverComponents were added
        for (int i = 0; i < newDriverDataArray.Length; ++i)
        {
            if (newDriverDataArray[i].isServer)
            {
                if (m_serverDriverIsActive)
                    throw new InvalidOperationException("Cannot create multiple server drivers");
                var drv = new UdpCNetworkDriver(new INetworkParameter[0]);
                if (drv.Bind(new IPEndPoint(IPAddress.Any, 9000)) != 0)
                    throw new Exception("Failed to bind to port 9000");
                else
                    drv.Listen();
                ServerDriver = drv;
                m_serverDriverIsActive = true;
            }
            else
            {
                if (ClientDriver.IsCreated)
                    throw new InvalidOperationException("Cannot create multiple client drivers");
                ClientDriver = new UdpCNetworkDriver(new INetworkParameter[0]);
            }
            Debug.Log("commandBuffer.AddComponent(newDriverEntities[i], new MessageDriverStateComponent { isServer = newDriverDataArray[i].isServer })");
            commandBuffer.AddComponent(newDriverEntities[i], new MessageDriverStateComponent { isServer = newDriverDataArray[i].isServer });
        }

        newDriverDataArray.Dispose();
        newDriverEntities.Dispose();

        JobHandle clientDep = default;
        JobHandle serverDep = default;
        JobHandle acceptDep = default;
        JobHandle cleanUpDep = default;

        NativeArray<MessageDriverComponentData> driverComponentArray = m_driverQuery.ToComponentDataArray<MessageDriverComponentData>(Allocator.TempJob);
        NativeArray<Entity> driverEntityArray = m_driverQuery.ToEntityArray(Allocator.TempJob);
        // Go through and update all drivers, also accept all incoming connections for server drivers
        
        
        for (int i = 0; i < driverComponentArray.Length; ++i)
        {
            if (driverComponentArray[i].isServer)
            {

                // Schedule a chain with driver update, a job to accept all connections and finally a job to delete all invalid connections
                serverDep = ServerDriver.ScheduleUpdate();
                var acceptJob = new DriverAcceptJob
                { driver = ServerDriver, commandBuffer = commandBuffer };
                acceptDep = acceptJob.Schedule(serverDep);
                acceptDep.Complete();
                var cleanupJob = new DriverCleanupJob
                {
                    commandBuffer = commandBuffer.ToConcurrent()
                };
                cleanUpDep = cleanupJob.Schedule(m_serverConnectionQuery, acceptDep);
                cleanUpDep.Complete();
            }
            else
                clientDep = ClientDriver.ScheduleUpdate();
        }

        if(ClientUIBehaviour.ServerListening)
        {
            //Debug.Log("ServerListening and " + m_serverDriverIsActive);
        }

        if (ClientUIBehaviour.ServerListening && !m_serverDriverIsActive)
        {
            var serverEntity = commandBuffer.CreateEntity();
            Debug.Log("Creating server driver component data");
            commandBuffer.AddComponent(serverEntity, new MessageDriverComponentData { isServer = true });
        }

        else if (!ClientUIBehaviour.ServerListening && m_serverDriverIsActive)
        {
            for (int i = 0; i < driverComponentArray.Length; ++i)
            {
                if (driverComponentArray[i].isServer)
                {
                    commandBuffer.RemoveComponent<MessageDriverComponentData>(driverEntityArray[i]);
                }
            }
        }
        

        driverComponentArray.Dispose();
        driverEntityArray.Dispose();

        JobHandle combinedHandle = JobHandle.CombineDependencies(clientDep, serverDep, acceptDep);
        combinedHandle = JobHandle.CombineDependencies(combinedHandle, cleanUpDep);
        
        m_bufferSystem.AddJobHandleForProducer(combinedHandle);

        combinedHandle.Complete();

        return combinedHandle;
    }
}
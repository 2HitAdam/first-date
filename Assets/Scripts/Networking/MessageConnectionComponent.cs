﻿using Unity.Entities;
using Unity.Networking.Transport;

struct MessageServerConnectionComponentData : IComponentData
{
    public NetworkConnection connection;
}

struct MessageClientConnectionComponentData : IComponentData
{
    public NetworkConnection connection;
}
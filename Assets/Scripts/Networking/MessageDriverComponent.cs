﻿using System;
using Unity.Entities;

[Serializable]
// A component used to limit when Network drivers are created
// The MessageDriverSystem uses this to create the NetworkDriver
public struct MessageDriverComponentData : IComponentData
{
    public bool isServer;
}

[UnityEngine.DisallowMultipleComponent]
public class MessageDriverComponent : ComponentDataProxy<MessageDriverComponentData> { };
using System;
using System.Net;
using UnityEngine;
using Unity.Networking.Transport;
using Unity.Entities;
using System.Collections.Generic;

/* The PingClientUIBehaviour is responsible for displaying statistics of a
 running ping client as well as for starting and stopping the ping of a
 selected ip. */
public class ClientUIBehaviour : MonoBehaviour
{
    // The EndPoint the ping client should ping, will be a non-created end point when ping should not run.
    public static NetworkEndPoint ServerEndPoint { get; private set; }
    // Whether or not we should be listening, used to create/destroy server drivers
    public static bool ServerListening { get; private set; }

    public static int InputValue { get; private set; } = -1;

    // Update the ping statistics displayed in the ui. Should be called from the ping client every time a new ping is complete
    public static void UpdateClientStats(int id, int time, bool isActive, int activeNodeId)
    {
        m_clientDisplayData.assignedId = id;
        m_clientDisplayData.pingTime = time;
        m_clientDisplayData.isActive = isActive;
        m_clientDisplayData.activeNodeId = activeNodeId;
    }

    public static void UpdateServerStats(int activeClientId, int activeNodeId, int playerCount)
    {
        m_serverDisplayData.playerCount = playerCount;
        m_serverDisplayData.activeClientId = activeClientId;
        m_serverDisplayData.activeNodeId = activeNodeId;
    }

    public static void ClearInput()
    {
        InputValue = -1;
    }

    private struct ServerDisplayData
    {
        public int activeNodeId;
        public int activeClientId;
        public int playerCount;
    }
    private static ServerDisplayData m_serverDisplayData;

    private struct ClientDisplayData
    {
        public int assignedId;
        public int pingTime;
        public bool isActive;
        public int activeNodeId;
    }
    private static ClientDisplayData m_clientDisplayData;

    public struct OptionsDisplayData
    {
        public string text;
        public int nodeId;
    }
    private static List<OptionsDisplayData> m_optionsDisplayDataList = new List<OptionsDisplayData>();

    public static void SetOptions(List<OptionsDisplayData> optionDisplayDataArray)
    {
        m_optionsDisplayDataList = optionDisplayDataArray;
    }

    public struct DialogueDisplayData
    {
        public string text;
        public int speaker;
    }
    private static List<DialogueDisplayData> m_dialogueDisplayDataList = new List<DialogueDisplayData>();

    public static void SetDialogue(List<DialogueDisplayData> dialogueDisplayDataArray)
    {
        m_dialogueDisplayDataList = dialogueDisplayDataArray;
    }


    private string m_CustomIp = "";
    

    void Start()
    {
        ServerEndPoint = default(NetworkEndPoint);
    }

    void OnGUI()
    {

        UpdateClientUI();
        // the way this class is named makes this dumb to add here but whatever this is
        // basically just debug placeholders
        UpdateServerUI();
        
    }

    private void ShowOptions()
    {
        if (m_optionsDisplayDataList.Count > 0)
            GUILayout.Space(20);

        foreach (var option in m_optionsDisplayDataList)
        {
            if(GUILayout.Button(option.text))
            {
                InputValue = option.nodeId;
            }
        }
    }
    
    private void ShowDialogue()
    {
        if (m_dialogueDisplayDataList.Count > 0)
            GUILayout.Space(20);

        foreach (var dialogue in m_dialogueDisplayDataList)
        {
            GUILayout.Label(dialogue.speaker.ToString() + ": " + dialogue.text);
        }
    }

    private void UpdateClientUI()
    {
        bool toggleServerListening = ServerListening ? GUILayout.Button("Stop server") : GUILayout.Button("Start server");
        if (toggleServerListening) ServerListening = !ServerListening;

        if (!ServerEndPoint.IsValid)
        {

            if (GUILayout.Button("Start client"))
            {
                SetServerEndpoint();
            }

            m_CustomIp = GUILayout.TextField(m_CustomIp);

        }
        else
        {
            // Ping is running, display ui for stopping it
            if (GUILayout.Button("Stop client"))
            {
                ServerEndPoint = default(NetworkEndPoint);
            }
        }

        GUILayout.Space(40);
        GUILayout.Label("Client Id " + m_clientDisplayData.assignedId + ": " + m_clientDisplayData.pingTime + "ms");
        GUILayout.Label("Active Node Id: " + m_clientDisplayData.activeNodeId);
        GUILayout.Label("Is Active: " + m_clientDisplayData.isActive);

        ShowDialogue();

        if (m_clientDisplayData.isActive)
        {
            //GUILayout.Space(40);

            ShowOptions();
        }

    }

    private void UpdateServerUI()
    {
        GUILayout.Space(40);
        GUILayout.Label("Player Count: " + m_serverDisplayData.playerCount);
        GUILayout.Label("Active Client Id: " + m_serverDisplayData.activeClientId);
        GUILayout.Label("Active Node Id: " + m_serverDisplayData.activeNodeId);
    }

    private void SetServerEndpoint()
    {
        ushort port = 9000;
        if (string.IsNullOrEmpty(m_CustomIp))
            ServerEndPoint = new IPEndPoint(IPAddress.Loopback, port);
        else
        {
            string[] endpoint = m_CustomIp.Split(':');
            ushort newPort = 0;
            if (endpoint.Length > 1 && ushort.TryParse(endpoint[1], out newPort))
                port = newPort;

            Debug.Log($"Connecting to PingServer at {endpoint[0]}:{port}.");
            ServerEndPoint = new IPEndPoint(IPAddress.Parse(endpoint[0]), port);
        }
    }
}


﻿using Unity.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Jobs;
using Unity.Networking.Transport;
using UnityEngine;
using Unity.Burst;
using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

//[UpdateAfter(typeof(Update))]
[UpdateAfter(typeof(MessageEntityCommandBufferSystem))]
public class MessageServerSystem : JobComponentSystem
{
    private MessageDriverSystem m_serverDriverSystem;
    private EntityQuery m_serverConnections;
    private EntityQuery m_serverGameStateQuery;
    private EntityQuery m_serverGameStateUpdatesQuery;

    private MessageEntityCommandBufferSystem m_BufferSystem;

    private NativeArray<int> assignedIds = new NativeArray<int>(2, Allocator.Persistent);

    public struct GameplayServerStateSingleton
    {
        public Entity entity;
        public GameplayServerStateComponentData data;
    }

    public GameplayServerStateSingleton GameplayServerState;

    struct WriteValues {
        public int assignedId;
        public int activeClientId;
        public int activeNodeId;
        public int playerCount; // not actually written back to client, but written to buffer
    }

    protected override void OnCreate()
    {
        m_BufferSystem = World.GetOrCreateSystem<MessageEntityCommandBufferSystem>();
        m_serverDriverSystem = World.GetOrCreateSystem<MessageDriverSystem>();
        m_serverConnections = GetEntityQuery(new ComponentType[] { typeof(MessageServerConnectionComponentData) });
        m_serverGameStateQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(GameplayServerStateComponentData) },
            Any = new ComponentType[0],
            None = new ComponentType[] { typeof(GameplayServerStateUpdateComponentData) }
        });

        m_serverGameStateUpdatesQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(GameplayServerStateComponentData), typeof(GameplayServerStateUpdateComponentData) },
            Any = new ComponentType[0],
            None = new ComponentType[0]
        });
    }

    protected override void OnDestroy()
    {
        assignedIds.Dispose();
    }

    struct MessageJob : IJobForEachWithEntity<MessageServerConnectionComponentData>
    {
        public UdpCNetworkDriver.Concurrent driver;
        public NativeArray<int> assignedIds;
        public GameplayServerStateSingleton gameplayServerState;
        public EntityCommandBuffer.Concurrent commandBuffer;
        [DeallocateOnJobCompletion]
        public NativeArray<Entity> updateEntities;
        [DeallocateOnJobCompletion]
        public NativeArray<GameplayServerStateComponentData> updateGeneralStateData;
        [DeallocateOnJobCompletion]
        public NativeArray<GameplayServerStateUpdateComponentData> updateSendData;

        public void Execute(Entity entity, int index, ref MessageServerConnectionComponentData connectionComponentData)
        {
            NetworkEvent.Type cmd;
            DataStreamReader strm;

            // start with writing updates to the connection
            // not sure how well this will play with disconnect events, though
            for(int i = 0; i < updateGeneralStateData.Length; ++i)
            {
                var messageData = new DataStreamWriter(12, Allocator.Temp);
                messageData.Write(updateSendData[i].assignedId);
                messageData.Write(updateGeneralStateData[i].activeClientId);
                messageData.Write(updateGeneralStateData[i].activeNodeId);
                // returns an int, if you want to debug (0 is bad, as we discovered)
                driver.Send(connectionComponentData.connection, messageData);
                messageData.Dispose();
                commandBuffer.DestroyEntity(index, updateEntities[i]);
            }
            

            while ((cmd = driver.PopEventForConnection(connectionComponentData.connection, out strm)) != NetworkEvent.Type.Empty)
            {
                if (cmd == NetworkEvent.Type.Data)
                {
                    Debug.Log("Server data event");

                    var readerCtx = default(DataStreamReader.Context);
                    int assignedId = strm.ReadInt(ref readerCtx);
                    int inputSelection = strm.ReadInt(ref readerCtx);

                    var writeValues = new WriteValues
                    {
                        assignedId = assignedId,    // leave that the same
                        activeClientId = gameplayServerState.data.activeClientId,
                        activeNodeId = gameplayServerState.data.activeNodeId,
                        playerCount = gameplayServerState.data.playerCount
                    };

                    /* --- update game state logic --- */
                    if (assignedId == 0)
                    {
                        if (writeValues.playerCount >= 2)
                        {
                            Debug.Log("Too many players!");
                            writeValues.assignedId = 0;
                            // this will tell the client to disconnect
                            // go awn, get outta here!
                        }
                        else
                        {
                            writeValues.assignedId = writeValues.playerCount + 1;
                            writeValues.playerCount++;
                        }
                    }

                    if(writeValues.playerCount == 2 && writeValues.activeClientId == 0)
                    {
                        writeValues.activeClientId = 1;
                        writeValues.activeNodeId = 1;
                    }

                    // disregard messages from players acting out of turn
                    // might come up if response time to server is slow and they queue up a heap of inputs
                    if (inputSelection != 0 && assignedId == writeValues.assignedId)
                    {
                        // handle input and changing game state
                        // swap which player is selecting
                        writeValues.activeClientId = writeValues.activeClientId == 1 ? 2 : 1;

                        // set the active node to what they chose
                        writeValues.activeNodeId = inputSelection;
                    }

                    // update our server state
                    commandBuffer.SetComponent(index, gameplayServerState.entity, new GameplayServerStateComponentData
                    {
                        activeClientId = writeValues.activeClientId,
                        activeNodeId = writeValues.activeNodeId,
                        playerCount = writeValues.playerCount
                    });

                    // I really dislike all this duplication.... gotta be a better way
                    Entity updateEntity = commandBuffer.CreateEntity(index);
                    commandBuffer.AddComponent(index, updateEntity, new GameplayServerStateComponentData
                    {
                        activeClientId = writeValues.activeClientId,
                        activeNodeId = writeValues.activeNodeId,
                        playerCount = writeValues.playerCount
                    });
                    commandBuffer.AddComponent(index, updateEntity, new GameplayServerStateUpdateComponentData
                    {
                        assignedId = writeValues.assignedId
                    });

                    // update our debug output
                    ClientUIBehaviour.UpdateServerStats(writeValues.activeClientId, writeValues.activeNodeId, writeValues.playerCount);
                }
                else if (cmd == NetworkEvent.Type.Disconnect)
                {
                    Debug.Log("Disconnecting server");
                    connectionComponentData = new MessageServerConnectionComponentData { connection = default };
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        
        // TODO we could avoid checking the connections length further down if we moved this to its own system
        // because the system would only run if the deps existed, as we saw
        var commandBuffer = m_BufferSystem.CreateCommandBuffer();

        NativeArray<GameplayServerStateComponentData> serverStateArray = m_serverGameStateQuery.ToComponentDataArray<GameplayServerStateComponentData>(Allocator.TempJob);
        NativeArray<Entity> clientStateEntityArray = m_serverGameStateQuery.ToEntityArray(Allocator.TempJob);

        if (serverStateArray.Length > 0)
        {
            GameplayServerState.data = serverStateArray[0];
            GameplayServerState.entity = clientStateEntityArray[0];
        }
        else
        {
            var serverStateEntity = commandBuffer.CreateEntity();
            commandBuffer.AddComponent(serverStateEntity, new GameplayServerStateComponentData());
            return inputDeps; // yeah, we skip a frame here which is not ideal, but it'll do for now
        }
        serverStateArray.Dispose();
        clientStateEntityArray.Dispose();

        NativeArray<MessageServerConnectionComponentData> serverConnectionArray = m_serverConnections.ToComponentDataArray< MessageServerConnectionComponentData>(Allocator.TempJob);
        if(serverConnectionArray.Length > 0)
        {
            NativeArray<Entity> updateEntities = m_serverGameStateUpdatesQuery.ToEntityArray(Allocator.TempJob);
            NativeArray<GameplayServerStateComponentData> updateGeneralStateData = m_serverGameStateUpdatesQuery.ToComponentDataArray<GameplayServerStateComponentData>(Allocator.TempJob);
            NativeArray<GameplayServerStateUpdateComponentData> updateSendData = m_serverGameStateUpdatesQuery.ToComponentDataArray<GameplayServerStateUpdateComponentData>(Allocator.TempJob);

            var messageJob = new MessageJob
            {
                driver = m_serverDriverSystem.ServerDriver.ToConcurrent(),
                assignedIds = assignedIds,
                gameplayServerState = GameplayServerState,
                commandBuffer = commandBuffer.ToConcurrent(),
                updateEntities = updateEntities,
                updateGeneralStateData = updateGeneralStateData,
                updateSendData = updateSendData
            };
            inputDeps = messageJob.Schedule(m_serverConnections, inputDeps);
        }
        serverConnectionArray.Dispose();

        return inputDeps;
    }
}
﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Networking.Transport;
using UnityEngine;
using UdpCNetworkDriver = Unity.Networking.Transport.BasicNetworkDriver<Unity.Networking.Transport.IPv4UDPSocket>;

[UpdateAfter(typeof(MessageEntityCommandBufferSystem))]
public class MessageClientSystem : JobComponentSystem
{
    private MessageEntityCommandBufferSystem m_BufferSystem;
    private MessageDriverSystem m_DriverSystem;

    private EntityQuery m_connectionQuery;
    private EntityQuery m_driverQuery;
    private EntityQuery m_inputEventsQuery;
    private EntityQuery m_clientGameStateQuery;

    public struct GameplayClientStateSingleton
    {
        public Entity entity;
        public GameplayClientStateComponentData data;
    }

    public static GameplayClientStateSingleton GameplayClientState;

    private NativeArray<PendingMessage> m_pendingMessages;
    // TODO: this is only used client side, would be more readable as a struct
    private NativeArray<int> m_messageStats;

    struct PendingMessage
    {
        public int assignedId;
        public int inputSelection;
        public float time;
    }

    struct MessageJob : IJob
    {
        // TODO: absolutely gotta do something about this, baaad code smell to be passing
        // this much in
        public UdpCNetworkDriver driver;
        [DeallocateOnJobCompletion]
        public NativeArray<MessageClientConnectionComponentData> connections;
        [DeallocateOnJobCompletion]
        public NativeArray<Entity> connectionEntities;
        [DeallocateOnJobCompletion]
        public NativeArray<InputEventComponentData> inputs;
        [DeallocateOnJobCompletion]
        public NativeArray<Entity> inputEntities;
        public NetworkEndPoint serverEndpoint;
        public NativeArray<PendingMessage> pendingMessages;
        public NativeArray<int> messageStats;
        public float fixedTime;
        public EntityCommandBuffer commandBuffer;
        public GameplayClientStateSingleton gameplayClientState;

        public void Execute()
        {

            // we want to connect 
            // a) if we don't have a player id
            // b) if we're sending an input
            // ...is b actually still true? we're just keeping connections open now
            // can they time out? what then? I should really read up on this more

            if (serverEndpoint.IsValid && connections.Length == 0)
            {
                if (gameplayClientState.data.assignedId == 0 || inputs.Length > 0)
                {
                    Debug.Log("commandBuffer.AddComponent(clientConnection, new MessageClientConnectionComponentData { connection = driver.Connect(serverEndpoint) });");
                    var clientConnection = commandBuffer.CreateEntity();
                    commandBuffer.AddComponent(clientConnection, new MessageClientConnectionComponentData { connection = driver.Connect(serverEndpoint) });
                }
            }

            if (!serverEndpoint.IsValid && connections.Length > 0)
            {
                for (int con = 0; con < connections.Length; ++con)
                {
                    connections[con].connection.Disconnect(driver);
                    Debug.Log("commandBuffer.DestroyEntity(connectionEntities[con]);");
                    commandBuffer.DestroyEntity(connectionEntities[con]);
                }

                return;
            }

            bool stateUpdated = false;
            for (int con = 0; con < connections.Length; ++con)
            {
                /* --- receive & read --- */

                DataStreamReader strm;
                NetworkEvent.Type cmd;

                while ((cmd = connections[con].connection.PopEvent(driver, out strm)) != NetworkEvent.Type.Empty) {
                    if (cmd == NetworkEvent.Type.Connect)
                    {
                        Debug.Log("Connect");
                        pendingMessages[0] = new PendingMessage { assignedId = gameplayClientState.data.assignedId, inputSelection = 0, time = fixedTime };
                        var messageData = new DataStreamWriter(12, Allocator.Temp);
                        messageData.Write(gameplayClientState.data.assignedId);
                        messageData.Write(0);
                        connections[con].connection.Send(driver, messageData);
                        messageData.Dispose();
                    }
                    else if(cmd == NetworkEvent.Type.Data)
                    {
                        Debug.Log("Data event");
                        messageStats[1] = (int)((fixedTime - pendingMessages[0].time) * 1000);
                        var readerCtx = default(DataStreamReader.Context);
                        int assignedId = strm.ReadInt(ref readerCtx);
                        int activeClientId = strm.ReadInt(ref readerCtx);
                        int activeNodeId = strm.ReadInt(ref readerCtx);

                        if(gameplayClientState.data.assignedId == 0 && assignedId == 0)
                        {
                            // if it comes back without a proper id, it's an invalid connection
                            // maybe a third person trying to join
                            connections[con].connection.Disconnect(driver);
                            commandBuffer.DestroyEntity(connectionEntities[con]);
                        }
                        else
                        {
                            // update our game state

                            // this will technically work, but it feels rubbish. I should make the first int from the server
                            // tell us what "type" of message we're sending, and read it appropriately
                            // type = 0 (connect / assign id event) type = 1 (update game state)
                            int newAssignedId = gameplayClientState.data.assignedId == 0 ? assignedId : gameplayClientState.data.assignedId;

                            commandBuffer.SetComponent(gameplayClientState.entity, new GameplayClientStateComponentData
                            { 
                                assignedId = newAssignedId,
                                isActive = (activeClientId != 0 && activeClientId == newAssignedId),
                                activeNodeId = activeNodeId
                            });

                            stateUpdated = true;
                        }

                        /*
                         * stay connected
                         * 
                        Debug.Log("commandBuffer.DestroyEntity(connectionEntities[con]);");
                        connections[con].connection.Disconnect(driver);
                        commandBuffer.DestroyEntity(connectionEntities[con]);
                        */
                    }
                    else if(cmd == NetworkEvent.Type.Disconnect)
                    {
                        Debug.Log("commandBuffer.DestroyEntity(connectionEntities[con]);");
                        commandBuffer.DestroyEntity(connectionEntities[con]);
                    }
                }

                /* --- write & send --- */

                // only write if we know we don't have changes to the state singleton waiting in the command buffer
                if (stateUpdated) return;

                for(int i = 0; i < inputs.Length; ++i)
                {
                    // if we've queued up a bunch of them before receiving a response, do we actually want to send it all... ?

                    pendingMessages[0] = new PendingMessage { assignedId = gameplayClientState.data.assignedId, inputSelection = inputs[i].value, time = fixedTime };
                    var messageData = new DataStreamWriter(12, Allocator.Temp);
                    messageData.Write(gameplayClientState.data.assignedId);
                    messageData.Write(inputs[i].value); // TODO: change for input
                    connections[con].connection.Send(driver, messageData);
                    messageData.Dispose();

                    commandBuffer.DestroyEntity(inputEntities[i]);
                }
            }
        }        
    }

    protected override void OnCreate()
    {
        m_BufferSystem = World.GetOrCreateSystem<MessageEntityCommandBufferSystem>();
        m_DriverSystem = World.GetOrCreateSystem<MessageDriverSystem>();

        m_driverQuery = GetEntityQuery(new ComponentType[] { typeof(MessageDriverComponentData) });
        m_connectionQuery = GetEntityQuery(new ComponentType[] { typeof(MessageClientConnectionComponentData) });
        m_clientGameStateQuery = GetEntityQuery(new ComponentType[] { typeof(GameplayClientStateComponentData) });
        m_inputEventsQuery = GetEntityQuery(new ComponentType[] { typeof(InputEventComponentData) });

        m_pendingMessages = new NativeArray<PendingMessage>(64, Allocator.Persistent);
        m_messageStats = new NativeArray<int>(2, Allocator.Persistent);

    }

    protected override void OnDestroy()
    {
        m_pendingMessages.Dispose();
        m_messageStats.Dispose();
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        if (!m_DriverSystem.ClientDriver.IsCreated)
            return inputDeps;

        var commandBuffer = m_BufferSystem.CreateCommandBuffer();

        NativeArray<GameplayClientStateComponentData> clientStateArray = m_clientGameStateQuery.ToComponentDataArray<GameplayClientStateComponentData>(Allocator.TempJob);
        NativeArray<Entity> clientStateEntityArray = m_clientGameStateQuery.ToEntityArray(Allocator.TempJob);

        if (clientStateArray.Length > 0)
        {
            GameplayClientState.data = clientStateArray[0];
            GameplayClientState.entity = clientStateEntityArray[0];
        }
        else
        {
            var clientStateEntity = commandBuffer.CreateEntity();
            commandBuffer.AddComponent(clientStateEntity, new GameplayClientStateComponentData());
            return inputDeps; // yeah, we skip a frame here which is not ideal, but it'll do for now
        }
        clientStateArray.Dispose();
        clientStateEntityArray.Dispose();

        // update the placeholder ui
        ClientUIBehaviour.UpdateClientStats(GameplayClientState.data.assignedId, m_messageStats[1], GameplayClientState.data.isActive, GameplayClientState.data.activeNodeId);

        // maybe move this to its own system later
        if(ClientUIBehaviour.InputValue != -1)
        {
            commandBuffer.AddComponent(commandBuffer.CreateEntity(), new InputEventComponentData { value = ClientUIBehaviour.InputValue  });
            ClientUIBehaviour.ClearInput();
        }

        JobHandle handle = default;
        
        NativeArray<Entity> connectionEntities = m_connectionQuery.ToEntityArray(Allocator.TempJob);
        NativeArray<MessageClientConnectionComponentData> connectionDataArray = m_connectionQuery.ToComponentDataArray<MessageClientConnectionComponentData>(Allocator.TempJob);
        NativeArray<Entity> inputEntities = m_inputEventsQuery.ToEntityArray(Allocator.TempJob);
        NativeArray<InputEventComponentData> inputDataArray = m_inputEventsQuery.ToComponentDataArray<InputEventComponentData>(Allocator.TempJob);


        var messageJob = new MessageJob
        {
            driver = m_DriverSystem.ClientDriver,
            connections = connectionDataArray,
            connectionEntities = connectionEntities,
            inputs = inputDataArray,
            inputEntities = inputEntities,
            serverEndpoint = ClientUIBehaviour.ServerEndPoint,
            pendingMessages = m_pendingMessages,
            messageStats = m_messageStats,
            fixedTime = Time.fixedTime,
            commandBuffer = commandBuffer,
            gameplayClientState = GameplayClientState
        };

        handle = messageJob.Schedule(inputDeps);

        // I had to add this to prevent an error in the Unity Engine when stopping play mode where
        // OnDestroy() would try to deallocate native arrays before the job had completed
        // causing fun memory leaks!
        handle.Complete();
        
        return handle;

    }
}
﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;
using Unity.Mathematics;
using Unity.Transforms;
using static Unity.Mathematics.math;

[UpdateInGroup(typeof(PresentationSystemGroup))]
public class DialogueRenderSystem : JobComponentSystem
{
    private EntityQuery m_DialogueQuery;
    //private int renderTop = -40;
    //private int renderOffsetPerItem = 20;
    
    // [BurstCompile] can't use this while we're using UI class ref I think
    // I think we can't ref the dialogue data as a class for it either,
    // though maybe static makes it okay? Hesitant to pass in the full dictionary
    struct DialogueRenderSystemJob : IJobForEach<DialogueComponentData, DialogueRenderComponentData>
    {
        // Add fields here that your job needs to do its work.
        // For example,
        //    public float deltaTime;
        
        public void Execute([ReadOnly] ref DialogueComponentData data, [ReadOnly] ref DialogueRenderComponentData renderData)
        {
            // TODO: make something with DialogueData.loadedDialogueData[data.id]

        }
    }

    protected override void OnCreate()
    {
        m_DialogueQuery = GetEntityQuery(new ComponentType[] {
            typeof(DialogueComponentData), typeof(DialogueRenderComponentData)
        });
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        var job = new DialogueRenderSystemJob();
        
        // Assign values to the fields on your job here, so that it has
        // everything it needs to do its work when it runs later.
        // For example,
        //     job.deltaTime = UnityEngine.Time.deltaTime;
        
        
        
        // Now that the job is set up, schedule it to be run. 
        return job.Schedule(m_DialogueQuery, inputDeps);
    }
}
﻿using Unity.Burst;
using Unity.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using static Unity.Mathematics.math;
using UnityEngine.Experimental.PlayerLoop;

[UpdateInGroup(typeof(PresentationSystemGroup))]
[AlwaysUpdateSystem]
public class DialogueSystem : JobComponentSystem
{

    private int previousNode = 0;
    private MessageEntityCommandBufferSystem m_BufferSystem;
    private EntityQuery m_optionsQuery;
    private EntityQuery m_dialogueQuery;

    
    struct ClearOptionsJob : IJobForEachWithEntity<DialogueComponentData, DialogueActiveOptionComponentData>
    {
        public EntityCommandBuffer.Concurrent commandBuffer;

        public void Execute(Entity entity, int index, ref DialogueComponentData c0, ref DialogueActiveOptionComponentData c1)
        {
            commandBuffer.DestroyEntity(index, entity);
        }
    }

    protected override void OnCreate()
    {
        m_BufferSystem = World.GetOrCreateSystem<MessageEntityCommandBufferSystem>();

        m_optionsQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(DialogueComponentData), typeof(DialogueActiveOptionComponentData) },
            Any = new ComponentType[0],
            None = new ComponentType[0]
        });

        m_dialogueQuery = GetEntityQuery(new EntityQueryDesc
        {
            All = new ComponentType[] { typeof(DialogueComponentData), typeof(DialogueRenderComponentData) },
            Any = new ComponentType[0],
            None = new ComponentType[0]
        });
    }

    
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        /*var job = new DialogueSystemJob();
        
        return job.Schedule(this, inputDependencies);
        */

        var commandBuffer = m_BufferSystem.CreateCommandBuffer();

        // TODO: THIS IS ALL BAD AND TEMPORARY
        // make an entity stack of client state changes
        // and use the data of that in a job instead of doing this junk
        int currentNode = MessageClientSystem.GameplayClientState.data.activeNodeId;
        if (previousNode != currentNode)
        {
            var clearOptionsJob = new ClearOptionsJob { commandBuffer = commandBuffer.ToConcurrent() };

            var clearOptionsHandle = clearOptionsJob.Schedule(m_optionsQuery, inputDeps);
            clearOptionsHandle.Complete();

            previousNode = currentNode;

            var dialogueEntity = commandBuffer.CreateEntity();
            commandBuffer.AddComponent(dialogueEntity, new DialogueComponentData
            {
                id = currentNode
            });
            commandBuffer.AddComponent(dialogueEntity, new DialogueRenderComponentData
            {
                // yeah, definitely make the change stack. This is too annoying to work out here
                speaker = MessageClientSystem.GameplayClientState.data.isActive ? 2 : 1
            });

            foreach (var child in DataController.dialogueDataDictionary[currentNode].childNodeIds)
            {
                var optionEntity = commandBuffer.CreateEntity();
                commandBuffer.AddComponent(optionEntity, new DialogueComponentData
                {
                    id = child
                });
                commandBuffer.AddComponent(optionEntity, new DialogueActiveOptionComponentData
                {
                    isActiveOption = true
                });
            }
        }

        NativeArray<DialogueComponentData> dialogueDataComponentArray = m_dialogueQuery.ToComponentDataArray< DialogueComponentData >(Allocator.TempJob);
        NativeArray<DialogueRenderComponentData> dialogueRenderDataComponentArray = m_dialogueQuery.ToComponentDataArray<DialogueRenderComponentData>(Allocator.TempJob);
        NativeArray<DialogueComponentData> optionDataComponentArray = m_optionsQuery.ToComponentDataArray<DialogueComponentData>(Allocator.TempJob);

        List<ClientUIBehaviour.OptionsDisplayData> optionsDisplayData = new List<ClientUIBehaviour.OptionsDisplayData>();
        List<ClientUIBehaviour.DialogueDisplayData> dialogueDisplayData = new List<ClientUIBehaviour.DialogueDisplayData>();

        for (int i = 0; i < dialogueDataComponentArray.Length; ++i)
        {
            dialogueDisplayData.Add(new ClientUIBehaviour.DialogueDisplayData
            {
                speaker = dialogueRenderDataComponentArray[i].speaker,
                text = DataController.dialogueDataDictionary[dialogueDataComponentArray[i].id].text
            });
        }

        for (int i = 0; i < optionDataComponentArray.Length; ++i)
        {
            optionsDisplayData.Add(new ClientUIBehaviour.OptionsDisplayData
            {
                nodeId = optionDataComponentArray[i].id,
                text = DataController.dialogueDataDictionary[optionDataComponentArray[i].id].text
            });
        }

        ClientUIBehaviour.SetDialogue(dialogueDisplayData);
        ClientUIBehaviour.SetOptions(optionsDisplayData);

        dialogueDataComponentArray.Dispose();
        optionDataComponentArray.Dispose();
        dialogueRenderDataComponentArray.Dispose();

        return inputDeps;
        
    }
}